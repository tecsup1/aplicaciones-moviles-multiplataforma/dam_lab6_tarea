/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  FlatList,
  Image,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

//lab6:
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Video from 'react-native-video';
import {SearchBar} from 'react-native-elements';

import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

import IndexVideo from './IndexVideo';

// Componente item:
function Item({props, goDetails}) {
  var hero_description = '';
  if (props.description == '') {
    hero_description = 'Este heroe no tiene descripción.';
  } else {
    hero_description = props.description;
  }

  return (
    <TouchableOpacity
      style={styles.item}
      onPress={() => {
        goDetails(props);
      }}>
      <Image
        style={[styles.tinyLogo, {flex: 1.6}]}
        source={{uri: props.thumbnail.path + '/portrait_xlarge.jpg'}}
      />

      <View style={[styles.itemText, {flex: 4}]}>
        <Text style={styles.title}>{props.name}</Text>
        <Text numberOfLines={2} style={styles.description}>
          {hero_description}
        </Text>
      </View>
      <Image
        style={{height: 25, flex: 1, alignSelf: 'center'}}
        source={require('../images/next.png')}
      />
    </TouchableOpacity>
  );
}

function VideoComp({route, navigation}) {
  //item_received: this.props.route.params.item,
  return (
    <View style={{flex: 1, justifyContent: 'center', backgroundColor: 'black'}}>
      <Video
        source={{
          //uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
          uri: route.params.item.url,
        }}
        controls={true}
        //fullscreen={false}
        resizeMode="cover"
        style={{width: '100%', height: 255}}
      />
    </View>
  );
}

function SettingsStack(props) {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Settings} />
    </Stack.Navigator>
  );
}

const settingsList = [
  {
    icon: 'account',
    text: 'Account',
  },
  {
    icon: 'bell',
    text: 'Notifications',
  },
  {
    icon: 'eye',
    text: 'Appearance',
  },
  {
    icon: 'security',
    text: 'Privaty & Security',
  },
  {
    icon: 'headphones',
    text: 'Help and Support',
  },
  {
    icon: 'help',
    text: 'About',
  },
];

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //settingsList: settingsList,
      search: '',
    };
    this.arrayholder = [];
  }

  componentDidMount() {
    this.setState({
      dataSource: settingsList,
    });
    this.arrayholder = settingsList;
  }

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.text ? item.text.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      search: text,
    });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'lightgray'}}>
        <View
          style={{margin: 20, backgroundColor: 'white', flex: 1, padding: 10}}>
          <SearchBar
            round
            placeholder="Search for a setting..."
            lightTheme={{platform: 'default'}}
            onChangeText={text => this.SearchFilterFunction(text)}
            onClear={text => this.SearchFilterFunction('')}
            value={this.state.search}
          />
          <FlatList
            data={this.state.dataSource}
            //data={filtered}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{flexDirection: 'row', height: 50, paddingTop: 13}}>
                <MaterialCommunityIcons
                  name={item.icon}
                  color="gray"
                  size={30}
                  style={{flex: 1, marginLeft: 15}}
                />
                <Text style={{flex: 5}}>{item.text}</Text>
                <MaterialCommunityIcons
                  name="arrow-right-thick"
                  color="gray"
                  size={30}
                  style={{flex: 1}}
                />
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={() => (
              <View
                style={{height: 1, width: '100%', backgroundColor: '#000'}}
              />
            )}
          />
        </View>
      </View>
    );
  }
}

class ListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    this.setState({loading: true});

    const apikey = '9e3d66957e41d7db7f364dfe7bb5497f';
    const ts = 1;
    const hash = 'b889f3b4144bc3d153cf268be106095c';

    const urlmarvel = `https://gateway.marvel.com:443/v1/public/characters?apikey=9e3d66957e41d7db7f364dfe7bb5497f&ts=1&hash=b889f3b4144bc3d153cf268be106095c&limit=35`;

    const urlpokemon = 'https://pokeapi.co/api/v2/pokemon/?limit=20';

    fetch(urlmarvel, {
      method: 'GET',
    })
      .then(res => res.json())
      .then(resJson => {
        //console.warn(resJson.data);
        this.setState({
          data: resJson.data.results,
          //data: resJson.results,
          loading: false,
        });
      })
      .catch(error => {
        console.warn(error);
      });
  };

  goDetails = item => {
    this.props.navigation.navigate('DetailScreen', {
      item: item,
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <FlatList
          data={this.state.data}
          renderItem={({item}) => (
            <Item props={item} goDetails={this.goDetails} />
          )}
          ItemSeparatorComponent={() => (
            <View style={{height: 1, width: '100%', backgroundColor: '#000'}} />
          )}
        />
      </View>
    );
  }
}

function ListTabNavigator(params) {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Listado"
        component={ListScreen}
        options={{
          tabBarLabel: 'Listado',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="database" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Video"
        component={IndexVideo}
        options={{
          tabBarLabel: 'Video',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="video" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="SettingsStack"
        component={SettingsStack}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="settings" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 5,
    marginHorizontal: 16,
    flexDirection: 'row',
    borderRadius: 5,
  },
  itemText: {
    margin: 10,
    color: '#4C5257',
    //Solución 1 para que texto no se salga de los limites
    //width: 0,
    //flexGrow: 1,
    //flex: 1,

    //Solución 2 para que texto no se salga de los limites
    flexShrink: 1,
  },
  title: {
    fontSize: 20,
    color: '#4C5257',
    textShadowColor: 'black',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 12,
  },
  tinyLogo: {
    width: 100,
    height: 100,
    borderRadius: 70,
  },
  description: {},
});

export {ListTabNavigator, VideoComp};
