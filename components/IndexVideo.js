/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, Text, Button, Alert, FlatList, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const urlVideos = [
  {
    name: 'Conejo en pradera',
    url: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
  },
  {
    name: 'Video erroneo',
    url: 'otro video',
  },
];

class IndexVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
        <FlatList
          data={urlVideos}
          renderItem={({item}) => (
            <TouchableOpacity
              style={{
                backgroundColor: 'lightgreen',
                padding: 20,
                borderRadius: 15,
                margin: 5,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 7,
              }}
              onPress={() => {
                this.props.navigation.navigate('VideoComp', {
                  item: item,
                });
              }}>
              <Text style={{color: 'gray', fontSize: 20}}>
                Nombre del video: {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

export default IndexVideo;
